import { Config } from '@vkontakte/superappkit';
import { Connect } from '@vkontakte/superappkit';


let ConfigInitParams = {
    appId: '8035800', //  идентификатор приложения, созданного на платформе для разработчиков
    superAppToken: 'mM9hFm9VwXINOJ7BgE9f' // токен авторизации для сервиса
}

Config.init(ConfigInitParams); // настройка конфига

// По клику на любую часть окна открывается окно авторизации VK ID
document.body.onclick = function(e) {
    Connect.userVisibleAuth().then(onAuthData, onAuthError);
};

// В onAuthData callback-функции в аргументе data будет содержаться
// информация об авторизации
function onAuthData(data) {
    console.log(data);
}

// В случае ошибки, будет вызвана on AuthError callback-функция
// Поле error.code может иметь следующие значения из enum ERROR_CODES
// CONNECT_WINDOW_CLOSED = 7 - окно авторизации закрылось
// CONNECT_WINDOW_NOT_OPENED = 8 - окно авторизации не было открыто
// CONNECT_DOMAIN_NOT_ALLOWED= 9 - хост окна авторизации некорректный
// CONNECT_UNKNOWN_SDK_MESSAGE = 10 - неизвестное сообщение от SDK

function onAuthError(error) {
    console.error(error);
}
