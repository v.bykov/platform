<?php
$token = $_REQUEST['accessToken'];
$silentToken = $_REQUEST['silentToken'];
$uid = $_REQUEST['uid'];
$info = ($_REQUEST['info'] ?? 'false');
$result = [];
if ($info == 'true') {
    $url = "https://api.vk.com/method/auth.getProfileInfoBySilentToken";

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = [
        "Content-Type: application/x-www-form-urlencoded",
    ];
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $data = "v=5.131&token[]=$silentToken&access_token=$token&uuid[]=$uid&event[]=";

    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    $resp = curl_exec($curl);
    curl_close($curl);
    $result['getProfileInfoBySilentToken'] = json_decode($resp, true);

}
$url = "https://api.vk.com/method/auth.exchangeSilentAuthToken";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = [
    "Content-Type: application/x-www-form-urlencoded",
];
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$data = "v=5.131&token=$silentToken&access_token=$token&uuid=$uid";

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

$resp = curl_exec($curl);
curl_close($curl);
$result['exchangeSilentAuthToken'] = json_decode($resp, true);

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($result));

